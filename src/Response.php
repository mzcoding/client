<?php

namespace Stanislavboyko\Client;

use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Stanislavboyko\Client\Contract\ResponseInterface;


class Response implements ResponseInterface
{
    private $response;

    /**
     * Response constructor.
     * @param Response $response
     */
    public function __construct(GuzzleResponse $response)
    {
        $this->response = $response;
    }


    /**
     * @return \App\Classes\Client\Guzzle\Response
     */
    public function get(): GuzzleResponse
    {
        return $this->response;
    }

	/**
	 * @return mixed
	 */
	public function getBody()
	{
		 return json_decode($this->get()->getBody(), true);
	}
}