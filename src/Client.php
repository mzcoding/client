<?php
namespace Stanislavboyko\Client;

use Stanislavboyko\Client\Config;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client as GuzzleClient;

/**
 * @method getReasonPhrase()
 */
class Client
{
	protected $client, $config;
	protected $params, $headers, $auth = [];

    public function __construct(Config $config, bool $init = true)
    {
       $this->config  = $config->get();
       $this->headers = $config->guzzle()['headers'];

       if($init) {
          return $this->clientInit($config->guzzle());
       }
    }
   /**
     * Get current client instance
     *
     * @return null|\GuzzleHttp\Client
     */
    public function getClient(): ?\GuzzleHttp\Client
    {
        return $this->client;
    }

    public function clientInit(array $config = []): self
    {
       $this->client =  new \GuzzleHttp\Client($config);
       return $this;
    }

    /**
     * @param array $headers
     * @return Client
     */
   public function setHeaders(array $headers = []): Client
   {
       $this->headers = array_merge($this->headers, $headers);
       return $this;
   }
    /**
     * @return array
     */
   public function getHeaders(): array
   {
      return $this->headers;
   }
    /**
     * @param array $credentials
     * @return Client
     */
    public function setAuth(array $credentials = []): Client
    {
        $this->auth = $credentials;
        return $this;
    }

    /**
     * @return array
     */
    public function getAuth(): array
    {
        return (array)$this->auth;
    }

	/**
	 * @param string $url
	 * @param string $method
	 * @param array $params
	 * @return Response
	 */
	public function request(string $url, string $method = 'GET', array $params = []): Response
    {
       $options = [];
       $method = strtoupper($method);
       if(isset($params[0])) {
       	  $options['json'] = [$params[0]];
       	  unset($params[0]);
	   }
       if( $this->auth ) {
           $options['auth'] = $this->auth;
       }
       if((bool)$this->config['debug']) {
           $options['debug'] = (bool)$this->config['debug'];
       }

       $response = $this->client->request($method, $this->config['base_url'] . "/" . $url, $options);
       return new Response($response);
    }

}